# mediawiki-peg-rust
This project aims to develop a parser for mediawiki markdown on the basis of Parsing Expression Grammars. 
It currently features a generated parser and test generation from a specification document.

Although compatibility with the online MediaWiki markup is desirable and worked towards, this parser will focus on a useful subset of the markup language for now. Instead, providing useful error messages and encouraging semantic markup will be a priority.

## More documentation and description to follow, this still in testing and evaluation phase
